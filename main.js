/*
1. Initialize your app with a package.json using an npm command
2. Create a start command in your package.json with the contents "node app.js", or a dev command in your package.json with the contents "nodemon app.js"
3. Enter the following contents into a file and start your server with "npm start" or "npm run dev". HINT: **You might need to do
   something before your app is able to run.**
4. Uncomment the line that begins with // app.use(express.someMiddleware()) and replace someMiddleware with a built-in
   middleware function from express that parses incoming requests with JSON payloads and is based on body-parser
5. Increment the numRequests variable inside of numRequestsMiddleware before next() is called
6. GET "/" should respond with "You visited the API numRequests times" instead of "Coding interview 3!" (make sure an
   integer is displayed for the count)
7. GET "/cars" should respond with the array of cars
8. POST "/cars" should add a car to the cars array with an id and respond with a 201 status code
9. POST "/cars" should not add cars that don't have both a "name" and a "color" property
10. GET "/cars/:id" should respond with the car of the id passed in or a 404 if the car was not found
11. Add additional functionality to GET "/cars" endpoint so that I can filter cars by color. For example GET
    "/cars?color=red" should return all the cars which are red
*/
const express = require("express");

const app = express();

const port = 5000;

let id = 0;
const getId = () => ++id;
const cars = [
  {
    id: getId(),
    name: "Prius",
    color: "blue",
  },
  {
    id: getId(),
    name: "Corvette",
    color: "red",
  },
  {
    id: getId(),
    name: "Altima",
    color: "white",
  },
];
let numRequests = 0;
app.use(express.json());

app.use(function numRequestsMiddleware(req, res, next) {
  numRequests++;
  next();
});

app.get("/", (req, res) => {
  res.send(`You visited the API numRequests ${Number(numRequests)}`);
  console.log(req.query);
});

app.get("/cars", (req, res) => {
  res.json(cars);
});

app.post("/cars", (req, res) => {
  let { name, color } = req.body;

  if (name && color) {
    newCar = {
      id: getId(),
      name,
      color,
    };

    let newCarsArr = cars.push(newCar);
    res.status(201).json(cars);
  } else {
    res.status(400).send("Error, missing name or color property");
  }
});

app.get("/cars/:id", (req, res) => {
  let correctCar = cars.find((car) => car.id === Number(req.params.id));
  res.json(correctCar);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
